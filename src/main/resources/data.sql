INSERT INTO RAMO_ATIVIDADE (ID, NOME) values
	(1, 'indústria'),
	(2, 'comércio'),
	(3, 'serviços');
	
INSERT INTO CONCORRENTE (ID, NOME) values
	(1, 'Rede'),
    (2, 'GetNet'),
    (3, 'PagSeguro'),
    (4, 'Stone'),
    (5, 'Vero');
    
INSERT INTO TAXA_MINIMA (ID, VALUE, TIPO_PAGAMENTO, RAMO_ATIVIDADE_ID) values
	(1, 2, 0, 1),
	(2, 3, 1, 1),
	(3, 2.5, 0, 2),
	(4, 3.7, 1, 2),
	(5, 2.3, 0, 3),
	(6, 3.5, 1, 3);