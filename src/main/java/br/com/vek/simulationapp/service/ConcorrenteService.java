package br.com.vek.simulationapp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.vek.simulationapp.exception.ConcorrenteNotFoundException;
import br.com.vek.simulationapp.model.Concorrente;
import br.com.vek.simulationapp.repository.ConcorrenteRepository;

@Service
public class ConcorrenteService {
	
	@Autowired
	private ConcorrenteRepository repository;
	
	public List<Concorrente> getAllConcorrente() {
		return this.repository.findAll();
	}
	
	public Concorrente getById(Long id) {
		Optional<Concorrente> concorrenteOpt = this.repository.findById(id);
		
		if(!concorrenteOpt.isPresent()) {
			throw new ConcorrenteNotFoundException("Concorrente com o id " + id + " não encontrado");
		}
		
		return concorrenteOpt.get();
	}
	
}
