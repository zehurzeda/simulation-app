package br.com.vek.simulationapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.vek.simulationapp.model.Taxa;
import br.com.vek.simulationapp.repository.TaxaRepository;

@Service
public class TaxaService {
	
	@Autowired
	private TaxaRepository repository;
	
	public Taxa saveTaxa(Taxa entity) {
		return this.repository.save(entity);
	}
	
}
