package br.com.vek.simulationapp.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.vek.simulationapp.model.Concorrente;
import br.com.vek.simulationapp.model.Proposta;
import br.com.vek.simulationapp.model.PropostaDTO;
import br.com.vek.simulationapp.model.RamoAtividade;
import br.com.vek.simulationapp.model.SimulacaoDTO;
import br.com.vek.simulationapp.model.TaxaMinima;
import br.com.vek.simulationapp.model.TipoPagamentoEnum;
import br.com.vek.simulationapp.repository.PropostaRepository;

@Service
public class PropostaService {

	@Autowired
	private PropostaRepository repository;

	@Autowired
	private ConcorrenteService concorrenteService;

	@Autowired
	private RamoAtividadeService ramoAtividadeService;

	@Autowired
	private TaxaMinimaService taxaMinimaService;
	
	@Autowired
	private TaxaService taxaService;

	public SimulacaoDTO simulateProposta(PropostaDTO propostaDto) {
		Map<TipoPagamentoEnum, TaxaMinima> taxaMinimaMap = this.taxaMinimaService
				.getTaxaMinimaByRamoAtividadeId(propostaDto.getRamoAtividadeId());

		SimulacaoDTO simulacaoDTO = new SimulacaoDTO();
		simulacaoDTO.setIsValid(taxaMinimaMap.get(TipoPagamentoEnum.DEBITO).getValue() <= propostaDto.getDebito().getDesconto()
				&& taxaMinimaMap.get(TipoPagamentoEnum.CREDITO).getValue() <= propostaDto.getCredito().getDesconto());
		simulacaoDTO.setMinValueDebit(taxaMinimaMap.get(TipoPagamentoEnum.DEBITO).getValue());
		simulacaoDTO.setMinValueCredit(taxaMinimaMap.get(TipoPagamentoEnum.CREDITO).getValue());
		simulacaoDTO.setProposta(propostaDto);
		
		return simulacaoDTO;
	}
	
	public Proposta saveProposta(PropostaDTO propostaDto) {
		RamoAtividade ramoAtividade = ramoAtividadeService.getRamoAtividadeById(propostaDto.getRamoAtividadeId());
		Concorrente concorrente = concorrenteService.getById(propostaDto.getConcorrenteId());
		
		Proposta propostaToSave = new Proposta(propostaDto, ramoAtividade, concorrente);
		propostaToSave.setDebito(this.taxaService.saveTaxa(propostaDto.getDebito()));
		propostaToSave.setCredito(this.taxaService.saveTaxa(propostaDto.getCredito()));
		propostaToSave.setSaveDate(LocalDateTime.now());
		
		return this.repository.save(propostaToSave);
	}
	
	
	public List<Proposta> getAllAcceptedProposta() {
		return this.repository.findAllByIsAcceptedIsTrue();
	}
}
