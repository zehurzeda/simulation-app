package br.com.vek.simulationapp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.vek.simulationapp.exception.RamoAtividadeNotFoundException;
import br.com.vek.simulationapp.model.RamoAtividade;
import br.com.vek.simulationapp.repository.RamoAtividadeRepository;

@Service
public class RamoAtividadeService {
	
	@Autowired
	private RamoAtividadeRepository repository;
	
	public List<RamoAtividade> getAllRamoAtividade() {
		return this.repository.findAll();
	}
	
	public RamoAtividade getRamoAtividadeById(Long id) {
		Optional<RamoAtividade> ramoAtividadeOpt = this.repository.findById(id);
		
		if(!ramoAtividadeOpt.isPresent()) {
			throw new RamoAtividadeNotFoundException("RamoAtividade com o id " + id + " não encontrado");
		}
		
		return ramoAtividadeOpt.get();
	}
	
}
