package br.com.vek.simulationapp.service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.vek.simulationapp.model.TaxaMinima;
import br.com.vek.simulationapp.model.TipoPagamentoEnum;
import br.com.vek.simulationapp.repository.TaxaMinimaRepository;

@Service
public class TaxaMinimaService {
	
	@Autowired
	private TaxaMinimaRepository repository;
	
	public Map<TipoPagamentoEnum, TaxaMinima> getTaxaMinimaByRamoAtividadeId(Long ramoAtividadeId) {
		List<TaxaMinima> listTaxaMinima = this.repository.findAllByRamoAtividadeId(ramoAtividadeId);
		
		return listTaxaMinima.stream().collect(Collectors.toMap(TaxaMinima::getTipoPagamento, taxaMinima -> taxaMinima));
	}
	
}
