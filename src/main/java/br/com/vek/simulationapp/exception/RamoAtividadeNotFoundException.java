package br.com.vek.simulationapp.exception;

public class RamoAtividadeNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 3870623573885140981L;
	
	public RamoAtividadeNotFoundException(String errorMessage, Throwable err) {
		super(errorMessage, err);
	}
	
	public RamoAtividadeNotFoundException(String errorMessage) {
		super(errorMessage);
	}
	
}
