package br.com.vek.simulationapp.exception;

public class ConcorrenteNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -1443163208047334947L;
	
	public ConcorrenteNotFoundException(String errorMessage, Throwable err) {
		super(errorMessage, err);
	}
	
	public ConcorrenteNotFoundException(String errorMessage) {
		super(errorMessage);
	}
	
}
