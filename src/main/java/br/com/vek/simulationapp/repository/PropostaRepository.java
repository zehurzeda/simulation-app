package br.com.vek.simulationapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.vek.simulationapp.model.Proposta;

public interface PropostaRepository extends JpaRepository<Proposta, Long>{
	
	public List<Proposta> findAllByIsAcceptedIsTrue();
	
}
