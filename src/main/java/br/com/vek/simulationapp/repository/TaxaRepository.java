package br.com.vek.simulationapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.vek.simulationapp.model.Taxa;

public interface TaxaRepository extends JpaRepository<Taxa, Long>{

}
