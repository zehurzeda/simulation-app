package br.com.vek.simulationapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.vek.simulationapp.model.Concorrente;

public interface ConcorrenteRepository extends JpaRepository<Concorrente, Long>{

}
