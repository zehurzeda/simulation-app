package br.com.vek.simulationapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.vek.simulationapp.model.TaxaMinima;

public interface TaxaMinimaRepository extends JpaRepository<TaxaMinima, Long>{
	
	List<TaxaMinima> findAllByRamoAtividadeId(Long id);
	
}
