package br.com.vek.simulationapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.vek.simulationapp.model.RamoAtividade;

public interface RamoAtividadeRepository extends JpaRepository<RamoAtividade, Long>{

}
