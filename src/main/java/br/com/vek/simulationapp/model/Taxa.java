package br.com.vek.simulationapp.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Taxa {
	
	@Id
	@GeneratedValue
	private Long id;
	
	private Double taxaConcorrente;
	
	private Double desconto;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getTaxaConcorrente() {
		return taxaConcorrente;
	}

	public void setTaxaConcorrente(Double taxaConcorrete) {
		this.taxaConcorrente = taxaConcorrete;
	}

	public Double getDesconto() {
		return desconto;
	}

	public void setDesconto(Double desconto) {
		this.desconto = desconto;
	}
	
	
	
}
