package br.com.vek.simulationapp.model;

public class SimulacaoDTO {

	private Boolean isValid;

	private Double minValueDebit;

	private Double minValueCredit;

	private PropostaDTO proposta;

	public Boolean getIsValid() {
		return isValid;
	}

	public void setIsValid(Boolean isValid) {
		this.isValid = isValid;
	}

	public Double getMinValueDebit() {
		return minValueDebit;
	}

	public void setMinValueDebit(Double minValueDebit) {
		this.minValueDebit = minValueDebit;
	}

	public Double getMinValueCredit() {
		return minValueCredit;
	}

	public void setMinValueCredit(Double minValueCredit) {
		this.minValueCredit = minValueCredit;
	}

	public PropostaDTO getProposta() {
		return proposta;
	}

	public void setProposta(PropostaDTO proposta) {
		this.proposta = proposta;
	}

}
