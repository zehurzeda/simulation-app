package br.com.vek.simulationapp.model;

public class PropostaDTO {

	private String cpfCnpj;

	private String telefone;

	private String email;

	private Long ramoAtividadeId;

	private Long concorrenteId;

	private Taxa debito;

	private Taxa credito;

	private Boolean isAccepted;

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getRamoAtividadeId() {
		return ramoAtividadeId;
	}

	public void setRamoAtividadeId(Long ramoAtividadeId) {
		this.ramoAtividadeId = ramoAtividadeId;
	}

	public Long getConcorrenteId() {
		return concorrenteId;
	}

	public void setConcorrenteId(Long concorrenteId) {
		this.concorrenteId = concorrenteId;
	}

	public Taxa getDebito() {
		return debito;
	}

	public void setDebito(Taxa debito) {
		this.debito = debito;
	}

	public Taxa getCredito() {
		return credito;
	}

	public void setCredito(Taxa credito) {
		this.credito = credito;
	}

	public Boolean getIsAccepted() {
		return isAccepted;
	}

	public void setIsAccepted(Boolean isAccepted) {
		this.isAccepted = isAccepted;
	}
	
	
}
