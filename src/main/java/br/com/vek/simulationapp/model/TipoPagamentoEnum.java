package br.com.vek.simulationapp.model;

public enum TipoPagamentoEnum {
	DEBITO("Débito"),
	CREDITO("Crédito");
	
	public String nome;

	private TipoPagamentoEnum(String nome) {
		this.nome = nome;
	}
	
}
