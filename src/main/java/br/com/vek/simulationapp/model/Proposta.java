package br.com.vek.simulationapp.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Proposta {

	@Id
	@GeneratedValue
	private Long id;

	private String cpfCnpj;

	private String telefone;

	private String email;

	@ManyToOne
	private RamoAtividade ramoAtividade;

	@ManyToOne
	private Concorrente concorrente;

	@OneToOne
	private Taxa debito;

	@OneToOne
	private Taxa credito;

	private Boolean isAccepted;

	private LocalDateTime saveDate;

	public Proposta() {
		super();
	}

	public Proposta(PropostaDTO dto, RamoAtividade ramoAtividade, Concorrente concorrente) {
		super();
		this.cpfCnpj = dto.getCpfCnpj();
		this.telefone = dto.getTelefone();
		this.email = dto.getEmail();
		this.ramoAtividade = ramoAtividade;
		this.concorrente = concorrente;
		this.debito = dto.getDebito();
		this.credito = dto.getCredito();
		this.isAccepted = dto.getIsAccepted();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public RamoAtividade getRamoAtividade() {
		return ramoAtividade;
	}

	public void setRamoAtividade(RamoAtividade ramoAtividade) {
		this.ramoAtividade = ramoAtividade;
	}

	public Concorrente getConcorrente() {
		return concorrente;
	}

	public void setConcorrente(Concorrente concorrente) {
		this.concorrente = concorrente;
	}

	public Taxa getDebito() {
		return debito;
	}

	public void setDebito(Taxa debito) {
		this.debito = debito;
	}

	public Taxa getCredito() {
		return credito;
	}

	public void setCredito(Taxa credito) {
		this.credito = credito;
	}

	public Boolean getIsAccepted() {
		return isAccepted;
	}

	public void setIsAccepted(Boolean isAccepted) {
		this.isAccepted = isAccepted;
	}

	public LocalDateTime getSaveDate() {
		return saveDate;
	}

	public void setSaveDate(LocalDateTime saveDate) {
		this.saveDate = saveDate;
	}

}
