package br.com.vek.simulationapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.vek.simulationapp.model.Proposta;
import br.com.vek.simulationapp.model.PropostaDTO;
import br.com.vek.simulationapp.model.SimulacaoDTO;
import br.com.vek.simulationapp.service.PropostaService;

@RestController
@RequestMapping("/proposta")
public class PropostaController {

	@Autowired
	private PropostaService service;
	
	@RequestMapping(value = "/simulate", method = RequestMethod.POST)
	public SimulacaoDTO simulateProposta(@RequestBody PropostaDTO propostaDto) {
		return this.service.simulateProposta(propostaDto);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public Proposta saveProposta(@RequestBody PropostaDTO propostaDto) {
		return this.service.saveProposta(propostaDto);
	}
	
	@RequestMapping(value = "/accepted", method = RequestMethod.GET)
	public List<Proposta> getAllAcceptedPropostas() {
		return this.service.getAllAcceptedProposta();
	}
	
}
