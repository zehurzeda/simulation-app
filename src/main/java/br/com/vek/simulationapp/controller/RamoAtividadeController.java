package br.com.vek.simulationapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.vek.simulationapp.model.RamoAtividade;
import br.com.vek.simulationapp.service.RamoAtividadeService;

@RestController
@RequestMapping("/ramo-atividade")
public class RamoAtividadeController {
	
	@Autowired
	private RamoAtividadeService service;
	
	@RequestMapping(method = RequestMethod.GET)
	public List<RamoAtividade> getAllRamoAtividade() {
		return this.service.getAllRamoAtividade();
	}

}
