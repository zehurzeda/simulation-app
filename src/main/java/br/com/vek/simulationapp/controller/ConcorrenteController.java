package br.com.vek.simulationapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.vek.simulationapp.model.Concorrente;
import br.com.vek.simulationapp.service.ConcorrenteService;

@RestController
@RequestMapping("/concorrente")
public class ConcorrenteController {
	
	@Autowired
	private ConcorrenteService service;
	
	@RequestMapping(method = RequestMethod.GET)
	public List<Concorrente> getAllConcorrente() {
		return this.service.getAllConcorrente();
	}
	
}
